# Ids721 Week 7 Project

### Requirement
1. Ingest data into Vector database
2. Perform queries and aggregations
3. Visualize output

### Steps
#### Install Vector Database Qdrant
```
docker pull qdrant/qdrant
docker run -p 6333:6333 -p 6334:6334 -e QDRANT__SERVICE__GRPC_PORT="6334" qdrant/qdrant
```
![qdrant](img/qdrant.png)

#### Create New Rust Project
```
Cargo new mp7
cd mp7
```

#### Add Dependencies to Cargo.toml
```
qdrant-client = "1.8.0"
serde_json = "1.0"
tokio = { version = "1", features = ["rt-multi-thread"] }
anyhow = "1.0.0"
```

#### Edit src/main.rs
##### Ingest Data
```
let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection.to_string(),
            vector: vec![0.3, 0.5, 0.8, 0.6],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

    dbg!(search_result);
```
##### Perform Queries
```
let _ = client.delete_collection(collection).await;
    client
    .create_collection(&CreateCollection {
        collection_name: collection.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Dot.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    })
    .await?;

    let points = vec![
        PointStruct::new(
            1,
            vec![0.1, 0.1, 0.6, 0.7],
            json!(
                {"position": "Durham"}
            )
            .try_into()
            .unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.9, 0.1, 0.5, 0.1],
            json!(
                {"position": "Brooklyn"}
            )
            .try_into()
            .unwrap(),
        ),
    ];
    client
        .upsert_points_blocking(collection.to_string(), None, points, None)
        .await?;
```
##### Output
![output](img/result.png)