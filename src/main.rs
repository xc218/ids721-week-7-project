use qdrant_client::qdrant::{vectors_config::Config, VectorParams, VectorsConfig};
use qdrant_client::qdrant::PointStruct;
use qdrant_client::qdrant::SearchPoints;
use qdrant_client::qdrant::Distance;
use qdrant_client::qdrant::CreateCollection;
use qdrant_client::client::QdrantClient;
use anyhow::Result;
use serde_json::json;

#[tokio::main]
async fn main() -> Result<()> {
    let client = QdrantClient::from_url("http://localhost:6334").build()?;
    let collection = "my_collection";

    let _ = client.delete_collection(collection).await;
    client
    .create_collection(&CreateCollection {
        collection_name: collection.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Dot.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    })
    .await?;

    let points = vec![
        PointStruct::new(
            1,
            vec![0.1, 0.1, 0.6, 0.7],
            json!(
                {"position": "Durham"}
            )
            .try_into()
            .unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.9, 0.1, 0.5, 0.1],
            json!(
                {"position": "Brooklyn"}
            )
            .try_into()
            .unwrap(),
        ),
    ];
    client
        .upsert_points_blocking(collection.to_string(), None, points, None)
        .await?;

    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection.to_string(),
            vector: vec![0.3, 0.5, 0.8, 0.6],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

    dbg!(search_result);

    Ok(())
}
